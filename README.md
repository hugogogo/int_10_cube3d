# cube3D
---

### install
---

this project uses submodules recursively, so you after cloning you need to :

`git submodule update --init --recursive`


### presentation
---

This project is a 3D mini game, that uses raycasting to produce a FPP (first person perspective), something like Wolfenstein3D or Doom.

![moving into the 3D](assets/cube3d_aller_retour.gif)

It creates a 3D view from a map in a text file, and move the view as if we were walking and looking around.

An example of a map :

```
$> cat -e map.cub
111111111111111111111111$
1......................1$
1......................1$
1......................1$
1.....11111....1.1.1...1$
1.....1...1............1$
1.....1...1....1...1...1$
1.....1...1....E.......1$
1.....11.11....1.1.1...1$
1......................1$
1......................1$
1......................1$
1......................1$
1......................1$
1.......11.............1$
1.......11.............1$
111111..11............11$
11.1....11.............1$
11....1.11............11$
11.1....11.............1$
11.1111111............11$
11...................111$
1111111111..........1111$
111111111111111111111111$
$>
```

This map will produce this 3D view :

![moving into the 3D](assets/cube3d_tour.gif)

You can change the images on the wall :

![change the images on the wall](assets/cube3d_image.gif)

And change the map itself :

![change the map](assets/cube3d_map.gif)


# raycasting
---

Raycasting is a technic that creates the illusion of 3D with low computational power.

This technic has the hability to not show hidden area by design, which is a great simplification for rendering.

How it works :

1. first you determine the position and orientation of the viewer in the map, and the distance and width of the screen

![map with viewer position](assets/raycast_1.png)

2. then you will scan this fictif screen with rays, each ray will correspond to a column in the 3D view

![animation of rays scanning the map and creating columns in 3D view](assets/raycast_2c.gif)

3. when a ray reaches a wall, it draw it in the 3D view

![example of one ray drawing one column with portion of wall](assets/raycast_3.png)

4. the height of the wall is inversely proportional to the length of the ray : the shorter the ray, the higher the wall

![example of two rays drawing two columns with portion of different height walls](assets/raycast_4.png)

5. so a full scan gives the illusion of perspective

![animation of rays scanning the whole map and creating a 3D with fish eye deformation](assets/raycast_5.gif)

6. but this gives rounded walls, like a fish eye effect, because rays change length while scanning the wall, so we must apply a correction according to the angle

![animation of rays scanning the whole map and creating a 3D with fish eye correction](assets/raycast_6.gif)

7. another step by step example to show how drawing gains in precision with more rays :

![animation of raytracing with increasing number of rays](assets/raycast_7.gif)

8. example of raycasting in action, with errors, during the construction phase of the raycasting algorithm :

![real raycasting during the construction  phase of the project](assets/cube3d_raycasting.gif)



# ressources
---

- [tuto mlx](https://harm-smits.github.io/42docs/libs/minilibx/getting_started.html)
- [tuto raycasting js](http://www.playfuljs.com/a-first-person-engine-in-265-lines)
- [course about matrices and transformations in space (chapter 1 to 5)](https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)


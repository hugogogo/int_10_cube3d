# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
#               . name = value \          . += append to a variable   #
#   VARIABLES   .        value            . != set result of command  #
#               . name is case sensitive  . ?= set if not already set #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

NAME      = cube3d
VPATH     = $(D_SRCS)
EXT       = c
CC        = gcc
OS        = $(shell uname)

# sources
D_SRCS    = srcs \
			srcs/init \
			srcs/gnl \
			srcs/parsing \
			srcs/hook \
			srcs/draw \
			srcs/player \
			srcs/mem

SRCS      = cube3d.c
# mem/
SRCS     += memorybook.c \
			memorybook_2d.c \
			memorybook_utils.c
# init/
SRCS     += init_struct.c \
			init_textures.c \
			init_game.c
# gnl/
SRCS     += get_next_line.c \
			get_next_line_utils.c
# parsing/
SRCS     += init_parsing.c \
			check_extension.c \
			check_path.c \
			check_rgb.c \
			check_map_borders.c \
			check_map_content.c
# hook/
SRCS     += keyhook.c \
			key_action_1.c \
			key_action_2.c
# player/
SRCS     += player_moves.c \
			player_rotates.c \
			player_limits.c
# draw/
SRCS     += draw.c \
			ray_intersect.c \
			raycast.c \
			draw_column.c

# headers
D_HEADERS = headers
HEADERS   = cube3d.h \
			cube3d_proto.h \
			cube3d_macro.h \
			cube3d_struct.h \
			memorybook.h \
			colors.h

INCLUDES  = -I$(D_HEADERS) -I$(D_LFT) -I$(D_LMLX)

# libraries
D_LIB     = libs
D_LFT     = $(D_LIB)/libft
ifeq (${OS}, Linux)
D_LMLX    = $(D_LIB)/minilibx-linux
else
D_LMLX    = $(D_LIB)/minilibx-macos
endif

# objects
D_OBJS    = builds
OBJS      = $(SRCS:%.$(EXT)=$(D_OBJS)/%.o)
RM_OBJS   = rm -rf $(D_OBJS)

# flags
CFLAGS    = -Wall -Wextra -Werror $(INCLUDES)
CFLAGS   += -g3
#CFLAGS   += -fsanitize=address
LFLAGS    = -L$(D_LFT) -lft
LFLAGS   += -L$(D_LMLX) -lm -lmlx -lXext -lX11


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #
#             .   target: prerequisites   .   $@ : target             #
#    RULES    .           recipe          .   $< : 1st prerequisite   #
#             .           recipe          .   $^ : all prerequisites  #
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

all: $(NAME)

$(D_OBJS)/%.o: %.$(EXT) | $(D_OBJS)
	$(CC) $(CFLAGS) -c $< -o $@

$(D_OBJS):
	mkdir $@

$(OBJS): $(HEADERS:%=$(D_HEADERS)/%)

$(NAME): $(OBJS)
	make -C $(D_LFT)
	make -C $(D_LMLX)
	$(CC) $(OBJS) -o $@ $(LFLAGS) $(CFLAGS)

leaks: $(NAME)
	valgrind --leak-check=full --show-leak-kinds=all ./$(NAME) maps/map_valid_03.cub

clean:
	$(RM_OBJS)

fclean: clean
	make fclean -C $(D_LFT)
	make clean -C $(D_LMLX)
	rm -f $(NAME)

re: fclean all

.PHONY : all clean fclean re


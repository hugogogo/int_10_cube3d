/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_textures.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:57:11 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:57:13 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

static void	init_txtr_img(t_img *img, void *mlx_ptr, char *path)
{
	img->ptr = mlx_xpm_file_to_image(mlx_ptr, path, &img->width, &img->height);
	img->data = mlx_get_data_addr(img->ptr, &img->bpp, &img->szl, &img->ndn);
}

void	init_txtr(t_txt *txt, void *mlx_ptr)
{
	init_txtr_img(&txt->img_n, mlx_ptr, txt->txt_north);
	init_txtr_img(&txt->img_s, mlx_ptr, txt->txt_south);
	init_txtr_img(&txt->img_e, mlx_ptr, txt->txt_east);
	init_txtr_img(&txt->img_w, mlx_ptr, txt->txt_west);
}

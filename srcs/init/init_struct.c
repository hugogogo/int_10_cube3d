/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_struct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:57:22 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 21:57:53 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

static void	init_null_mlx_ptr(t_game *game)
{
	game->img.ptr = NULL;
	game->txt.img_n.ptr = NULL;
	game->txt.img_s.ptr = NULL;
	game->txt.img_e.ptr = NULL;
	game->txt.img_w.ptr = NULL;
	game->win.ptr = NULL;
	game->mlx_ptr = NULL;
}

static void	init_map(t_map *map)
{
	map->content = NULL;
	map->tmp_str = NULL;
	map->cell = CELL;
	map->size_x = 0;
	map->size_y = 0;
	map->plr_x = 0;
	map->plr_y = 0;
}

static void	init_txt_null(t_txt *txt)
{
	txt->txt_north = NULL;
	txt->txt_south = NULL;
	txt->txt_east = NULL;
	txt->txt_west = NULL;
}

t_game	*init_struct(void)
{
	t_game	*game;

	game = mb_alloc(sizeof(t_game));
	init_map(&(game->map));
	init_txt_null(&(game->txt));
	init_null_mlx_ptr(game);
	game->fd = -1;
	return (game);
}

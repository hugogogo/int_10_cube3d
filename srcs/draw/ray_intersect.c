/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_intersect.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:52:25 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:52:27 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

static void	init_ray_params(t_rcast *rcast, t_vec *ray)
{
	rcast->ray_sign_x = 1;
	if (ray->start.x < ray->end.x)
		rcast->ray_sign_x = -1;
	rcast->ray_sign_y = 1;
	if (ray->start.y < ray->end.y)
		rcast->ray_sign_y = -1;
	rcast->cell_x = ray->start.x / rcast->cell;
	rcast->cell_y = ray->start.y / rcast->cell;
	rcast->slope_x = ray->end.x - ray->start.x;
	rcast->slope_y = ray->end.y - ray->start.y;
	rcast->next_cell_x = -rcast->ray_sign_x;
	rcast->next_cell_y = -rcast->ray_sign_y;
}

static void	init_first_step(t_rcast *rcast, t_vec *ray)
{
	rcast->first_next_x = 0;
	if (rcast->slope_x)
		rcast->first_next_x = ray->start.x % rcast->cell;
	if (rcast->ray_sign_x < 0)
		rcast->first_next_x -= rcast->cell;
	rcast->first_next_y = 0;
	if (rcast->slope_y)
		rcast->first_next_y = ray->start.y % rcast->cell;
	if (rcast->ray_sign_y < 0)
		rcast->first_next_y -= rcast->cell;
	rcast->next_x = ft_abs(rcast->first_next_x);
	if (rcast->slope_y != 0)
		rcast->next_x *= ft_abs(rcast->slope_y);
	rcast->next_y = ft_abs(rcast->first_next_y);
	if (rcast->slope_x != 0)
		rcast->next_y *= ft_abs(rcast->slope_x);
}

static void	init_steps(t_rcast *rcast)
{
	rcast->ray_step_x = 0;
	if (rcast->slope_x)
	{
		rcast->ray_step_x = ft_abs(rcast->cell);
		if (rcast->slope_y)
			rcast->ray_step_x *= ft_abs(rcast->slope_y);
	}
	rcast->ray_step_y = 0;
	if (rcast->slope_y)
	{
		rcast->ray_step_y = ft_abs(rcast->cell);
		if (rcast->slope_x)
			rcast->ray_step_y *= ft_abs(rcast->slope_x);
	}
}

static void	next_cell(t_rcast *rcast)
{
	if (!rcast->slope_x || (rcast->next_x > rcast->next_y && rcast->slope_y))
	{
		rcast->cell_y += rcast->next_cell_y;
		rcast->next_y += rcast->ray_step_y;
		rcast->is_x = 0;
	}
	else
	{
		rcast->cell_x += rcast->next_cell_x;
		rcast->next_x += rcast->ray_step_x;
		rcast->is_x = 1;
	}
}

void	ray_intersect_wall(t_game *game, t_rcast *rcast, t_vec *ray)
{
	ray->start.x = rcast->ray.start.x + game->plr.pos.x;
	ray->start.y = rcast->ray.start.y + game->plr.pos.y;
	ray->end.x = rcast->ray.end.x + game->plr.pos.x;
	ray->end.x += rcast->ray_nb;
	ray->end.y = rcast->ray.end.y + game->plr.pos.y;
	rotate(&(game->plr), &(ray->end));
	init_ray_params(rcast, ray);
	init_first_step(rcast, ray);
	init_steps(rcast);
	while (!is_wall(game, rcast->cell_x, rcast->cell_y))
		next_cell(rcast);
}

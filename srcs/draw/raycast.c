/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycast.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:54:22 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:54:23 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

static void	calcul_img_column(t_game *game, t_rcast *rcast, t_wall *wall)
{
	int	tmp;

	if (rcast->is_x == 1)
	{
		tmp = wall->delta;
		tmp *= -rcast->ray_sign_y;
		tmp /= ft_abs(rcast->slope_x);
		tmp += game->plr.pos.y;
		wall->posx = tmp % rcast->cell;
	}
	else
	{
		tmp = wall->delta;
		tmp *= -rcast->ray_sign_x;
		tmp /= ft_abs(rcast->slope_y);
		tmp += game->plr.pos.x;
		wall->posx = tmp % rcast->cell;
	}
}

static void	fill_wall_vector(t_rcast *rcast, t_wall *wall)
{
	int	height;

	height = wall->height;
	if (height < 0)
		height = 0;
	wall->limit = 0;
	if (height > rcast->screen_height)
	{
		wall->limit = (height - rcast->screen_height) / 2;
		height = rcast->screen_height;
	}
	rcast->wall.vec.start.y = rcast->screen_height / 2 + height / 2;
	rcast->wall.vec.end.y = rcast->screen_height / 2 - height / 2;
}

static void	calcul_wall(t_rcast *rcast)
{
	long int	height;

	rcast->wall.vec.start.x = rcast->ray_nb;
	rcast->wall.vec.end.x = rcast->ray_nb;
	if (rcast->is_x == 1)
		rcast->wall.delta = rcast->next_x - rcast->ray_step_x;
	else
		rcast->wall.delta = rcast->next_y - rcast->ray_step_y;
	height = rcast->screen_height * rcast->cell;
	if (rcast->slope_x)
		height *= rcast->slope_x;
	if (rcast->slope_y)
		height *= rcast->slope_y;
	if (rcast->wall.delta && rcast->screen_dist)
		height /= (rcast->wall.delta * rcast->screen_dist);
	rcast->wall.height = ft_abs(height);
	fill_wall_vector(rcast, &rcast->wall);
}

void	raycast(t_game *game, t_rcast *rcast)
{
	t_vec	ray;

	rcast->ray_nb = 0;
	while (rcast->ray_nb <= rcast->screen_width)
	{
		ray_intersect_wall(game, rcast, &ray);
		calcul_wall(rcast);
		calcul_img_column(game, rcast, &rcast->wall);
		draw_column(game, rcast, &rcast->wall, &game->txt);
		(rcast->ray_nb)++;
	}
}

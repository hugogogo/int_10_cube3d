/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_moves.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:50:18 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:50:21 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

void	plr_posx_decrement(t_game *game, t_plr *plr)
{
	t_d_coord	pos;
	int			limit_x;
	int			limit_y;
	int			limit_xy;

	pos.x = plr->exact.x - PLR_MV;
	pos.y = plr->exact.y;
	rotate_double(plr, &pos);
	limit_x = plr_out_limits(game, pos.x, plr->pos.y);
	limit_y = plr_out_limits(game, plr->pos.x, pos.y);
	limit_xy = plr_out_limits(game, pos.x, pos.y);
	if (limit_x)
		pos.x = plr->exact.x;
	if (limit_y)
		pos.y = plr->exact.y;
	if (limit_xy && !limit_y && !limit_x)
	{
		pos.x = plr->exact.x;
		pos.y = plr->exact.y;
	}
	plr->exact.x = pos.x;
	plr->exact.y = pos.y;
	(plr->pos.x) = (int)(pos.x);
	(plr->pos.y) = (int)(pos.y);
}

void	plr_posy_decrement(t_game *game, t_plr *plr)
{
	t_d_coord	pos;
	int			limit_x;
	int			limit_y;
	int			limit_xy;

	pos.x = plr->exact.x;
	pos.y = plr->exact.y - PLR_MV;
	rotate_double(plr, &(pos));
	limit_x = plr_out_limits(game, pos.x, plr->pos.y);
	limit_y = plr_out_limits(game, plr->pos.x, pos.y);
	limit_xy = plr_out_limits(game, pos.x, pos.y);
	if (limit_x)
		pos.x = plr->exact.x;
	if (limit_y)
		pos.y = plr->exact.y;
	if (limit_xy && !limit_y && !limit_x)
	{
		pos.x = plr->exact.x;
		pos.y = plr->exact.y;
	}
	plr->exact.x = pos.x;
	plr->exact.y = pos.y;
	(plr->pos.x) = (int)(pos.x);
	(plr->pos.y) = (int)(pos.y);
}

void	plr_posx_increment(t_game *game, t_plr *plr)
{
	t_d_coord	pos;
	int			limit_x;
	int			limit_y;
	int			limit_xy;

	pos.x = plr->exact.x + PLR_MV;
	pos.y = plr->exact.y;
	rotate_double(plr, &(pos));
	limit_x = plr_out_limits(game, pos.x, plr->pos.y);
	limit_y = plr_out_limits(game, plr->pos.x, pos.y);
	limit_xy = plr_out_limits(game, pos.x, pos.y);
	if (limit_x)
		pos.x = plr->exact.x;
	if (limit_y)
		pos.y = plr->exact.y;
	if (limit_xy && !limit_y && !limit_x)
	{
		pos.x = plr->exact.x;
		pos.y = plr->exact.y;
	}
	plr->exact.x = pos.x;
	plr->exact.y = pos.y;
	(plr->pos.x) = (int)(pos.x);
	(plr->pos.y) = (int)(pos.y);
}

void	plr_posy_increment(t_game *game, t_plr *plr)
{
	t_d_coord	pos;
	int			limit_x;
	int			limit_y;
	int			limit_xy;

	pos.x = plr->exact.x;
	pos.y = plr->exact.y + PLR_MV;
	rotate_double(plr, &(pos));
	limit_x = plr_out_limits(game, pos.x, plr->pos.y);
	limit_y = plr_out_limits(game, plr->pos.x, pos.y);
	limit_xy = plr_out_limits(game, pos.x, pos.y);
	if (limit_x)
		pos.x = plr->exact.x;
	if (limit_y)
		pos.y = plr->exact.y;
	if (limit_xy && !limit_y && !limit_x)
	{
		pos.x = plr->exact.x;
		pos.y = plr->exact.y;
	}
	plr->exact.x = pos.x;
	plr->exact.y = pos.y;
	(plr->pos.x) = (int)(pos.x);
	(plr->pos.y) = (int)(pos.y);
}

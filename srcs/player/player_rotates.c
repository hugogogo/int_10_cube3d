/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_rotates.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:49:40 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:49:57 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

void	rotate(t_plr *plr, t_coord *coord)
{
	int		old_x;
	t_coord	tmp;

	if (plr->rot == 0)
		return ;
	tmp.x = coord->x - plr->pos.x;
	tmp.y = coord->y - plr->pos.y;
	old_x = tmp.x;
	tmp.x = tmp.x * plr->cosi + tmp.y * plr->cosj;
	tmp.y = old_x * plr->sini + tmp.y * plr->sinj;
	coord->x = tmp.x + plr->pos.x;
	coord->y = tmp.y + plr->pos.y;
}

void	rotate_double(t_plr *plr, t_d_coord *coord)
{
	double		old_x;
	t_d_coord	tmp;

	if (plr->rot == 0)
		return ;
	tmp.x = coord->x - plr->exact.x;
	tmp.y = coord->y - plr->exact.y;
	old_x = tmp.x;
	tmp.x = tmp.x * plr->cosi + tmp.y * plr->cosj;
	tmp.y = old_x * plr->sini + tmp.y * plr->sinj;
	coord->x = tmp.x + plr->exact.x;
	coord->y = tmp.y + plr->exact.y;
}

void	plr_turn(t_plr *plr, int deg)
{
	double	radi;
	double	radj;

	if (plr->rot == -180)
		(plr->rot) *= -1;
	plr->rot += deg;
	radi = plr->rot * M_PI / 180;
	radj = radi + (M_PI / 2);
	plr->cosi = cos(radi);
	plr->sini = sin(radi);
	plr->cosj = cos(radj);
	plr->sinj = sin(radj);
}

void	plr_turn_left(t_plr *plr)
{
	plr_turn(plr, -plr->deg);
}

void	plr_turn_right(t_plr *plr)
{
	plr_turn(plr, plr->deg);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_limits.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:50:31 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:50:37 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

int	is_wall(t_game *game, int cell_x, int cell_y)
{
	if (cell_x < 0 || cell_y < 0)
		return (1);
	if (cell_x > game->map.size_x || cell_y > game->map.size_y)
		return (1);
	if (game->map.content[cell_y][cell_x] != '0')
		return (1);
	return (0);
}

int	plr_out_limits(t_game *game, int x, int y)
{
	int	xmax;
	int	ymax;
	int	cell_x;
	int	cell_y;

	xmax = game->map.size_x * game->map.cell;
	ymax = game->map.size_y * game->map.cell;
	cell_x = x / game->map.cell;
	cell_y = y / game->map.cell;
	if (is_wall(game, cell_x, cell_y))
		return (1);
	if (x < 0 || y < 0 || x > xmax || y > ymax)
		return (1);
	return (0);
}

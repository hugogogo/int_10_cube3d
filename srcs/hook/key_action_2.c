/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_action_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:48:54 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:48:56 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

int	is_turn_left(int *k_hook, int *is_action)
{
	if (!ft_arrint(k_hook, ARROW_LEFT, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	return (0);
}

int	is_turn_right(int *k_hook, int *is_action)
{
	if (!ft_arrint(k_hook, ARROW_RIGHT, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	return (0);
}

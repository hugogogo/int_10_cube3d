/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_action_1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:48:28 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:49:27 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

int	is_esc(int *k_hook, int *is_action)
{
	if (!ft_arrint(k_hook, KEY_ESC, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	return (0);
}

int	is_go_left(int *k_hook, int *is_action)
{
	if (!ft_arrint(k_hook, KEY_A, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	if (!ft_arrint(k_hook, KEY_Q, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	return (0);
}

int	is_go_right(int *k_hook, int *is_action)
{
	if (!ft_arrint(k_hook, KEY_D, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	return (0);
}

int	is_go_forward(int *k_hook, int *is_action)
{
	if (!ft_arrint(k_hook, KEY_W, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	if (!ft_arrint(k_hook, KEY_Z, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	return (0);
}

int	is_go_backward(int *k_hook, int *is_action)
{
	if (!ft_arrint(k_hook, KEY_S, MAX_NB_KEY))
	{
		*is_action = 1;
		return (1);
	}
	return (0);
}

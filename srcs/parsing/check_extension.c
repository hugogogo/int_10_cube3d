/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_extension.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <pblagoje@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 00:24:35 by pblagoje          #+#    #+#             */
/*   Updated: 2022/04/17 00:24:38 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

int	check_extension(char *filename, char *ext)
{
	char	*str;

	if (!filename || !ext)
		return (1);
	str = ft_strrchr(filename, '.');
	if (!str)
		return (2);
	if (!ft_strcmp(str, ext))
		return (0);
	return (3);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_parsing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <pblagoje@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/15 16:03:50 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 21:57:24 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

static int	size_map(t_map *map, int fd)
{
	char	*line;
	int		ret;

	ret = ft_gnl(fd, &line);
	mb_add(line);
	while (ret > 0)
	{
		if (ret != 0 && map->size_x)
		{
			if (*line == '\n' || *line == '\0')
			{
				mb_free(line);
				return (EXIT_FAILURE);
			}
		}
		else if (*line != '\n' && (int)ft_strlen(line) > map->size_x)
		{
			map->size_x = ft_strlen(line);
			if (line[map->size_x - 1] != '\n')
				map->size_x++;
		}
		if (*line != '\n' && *line != '\0')
			map->size_y++;
		mb_free(line);
		line = NULL;
		ret = ft_gnl(fd, &line);
		mb_add(line);
	}
	return (EXIT_SUCCESS);
}

static int	find_map(t_map *map, char *file)
{
	int		fd;
	int		count;

	fd = open(file, O_RDONLY);
	count = 0;
	if (fd == -1)
		return (-1);
	while (count < TOTAL_ELEMENTS + 1)
	{
		ft_gnl(fd, &map->tmp_str);
		mb_add(map->tmp_str);
		if (!map->tmp_str)
		{
			close(fd);
			return (-1);
		}
		if (map->tmp_str[0] != '\n' && map->tmp_str[0] != '\0')
			count++;
		if (count < TOTAL_ELEMENTS + 1)
			mb_free(map->tmp_str);
	}
	return (fd);
}

static void	fill_row(char *row, char *line, int width)
{
	int		i;

	i = 0;
	while (line != NULL && line[i] != '\0' && line[i] != '\n')
	{
		if (line[i] == '.')
			row[i] = '0';
		else
			row[i] = line[i];
		i++;
	}
	while (i < width - 1)
	{
		row[i] = ' ';
		i++;
	}
	row[i] = '\0';
}

static void	fill_map(t_map *map, int fd)
{
	int	i;
	int	ret;

	ret = 1;
	map->content = (char **)mb_alloc((map->size_y + 1) * sizeof(char *));
	i = -1;
	while (++i < map->size_y)
	{
		if (i > 0)
		{
			ret = ft_gnl(fd, &map->tmp_str);
			mb_add(map->tmp_str);
		}
		map->content[i] = (char *)mb_alloc((map->size_x + 1) * sizeof(char));
		fill_row(map->content[i], map->tmp_str, map->size_x);
		mb_free(map->tmp_str);
	}
	map->content[i] = NULL;
	while (ret)
	{
		ret = ft_gnl(fd, &map->tmp_str);
		if (ret)
			mb_add(map->tmp_str);
	}
}

int	init_parsing(t_game *game, char *file)
{
	game->fd = check_elements(game, file);
	if (game->fd == -1)
		mb_exit("Error\nInvalid .cub file.\n", EXIT_FAILURE);
	if (size_map(&(game->map), game->fd))
		mb_exit("Error\nInvalid map format.\n", EXIT_FAILURE);
	close(game->fd);
	game->fd = -1;
	game->fd = find_map(&(game->map), file);
	if (game->fd == -1)
		mb_exit("Error\nCouldn't locate the map.\n", EXIT_FAILURE);
	fill_map(&(game->map), game->fd);
	close(game->fd);
	game->fd = -1;
	return (EXIT_SUCCESS);
}

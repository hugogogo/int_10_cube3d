/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <pblagoje@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/18 12:37:48 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:05:47 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

int	check_borders(t_map *map)
{
	int	x;
	int	y;

	y = 0;
	while (map->content[y] != NULL)
	{
		x = 0;
		while (map->content[y][x] != '\0' && map->content[y][x] != '\n')
		{
			if (y == 0 || y == map->size_y - 1)
				if (map->content[y][x] != '1' && map->content[y][x] != ' ')
					mb_exit("Error\nInvalid map borders.\n", EXIT_FAILURE);
			if (x == 0 || x == map->size_x - 1)
				if (map->content[y][x] != '1' && map->content[y][x] != ' ')
					mb_exit("Error\nInvalid map borders.\n", EXIT_FAILURE);
			x++;
		}
		y++;
	}
	return (EXIT_SUCCESS);
}

int	check_map(t_map *map)
{
	if (check_borders(map) || check_content(map))
		mb_exit("Error\nInvalid map content.\n", EXIT_FAILURE);
	return (EXIT_SUCCESS);
}

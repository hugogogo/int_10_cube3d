/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_path.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <pblagoje@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 20:54:37 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:01:16 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

int	set_path(t_txt *txt, char *path, char identifier)
{
	if (identifier == 'N' && txt->txt_north == NULL)
	{
		txt->txt_north = ft_strdup(path);
		mb_add(txt->txt_north);
		return (EXIT_SUCCESS);
	}
	else if (identifier == 'S' && txt->txt_south == NULL)
	{
		txt->txt_south = ft_strdup(path);
		mb_add(txt->txt_south);
		return (EXIT_SUCCESS);
	}
	else if (identifier == 'W' && txt->txt_west == NULL)
	{
		txt->txt_west = ft_strdup(path);
		mb_add(txt->txt_west);
		return (EXIT_SUCCESS);
	}
	else if (identifier == 'E' && txt->txt_east == NULL)
	{
		txt->txt_east = ft_strdup(path);
		mb_add(txt->txt_east);
		return (EXIT_SUCCESS);
	}
	return (EXIT_FAILURE);
}

static int	check_path(t_txt *txt, char *element, char identifier)
{
	char	*path;
	int		fd;

	while (element && *element && ft_strchr(" \t\r", *element))
		element++;
	path = ft_substr(element, 0, ft_strlen(element));
	mb_add(path);
	fd = open(path, O_RDONLY);
	if (fd == -1 || check_extension(path, ".xpm"))
	{
		mb_free(path);
		close(fd);
		mb_exit("Error\nInvalid texture file.\n", EXIT_FAILURE);
	}
	if (set_path(txt, path, identifier) == EXIT_FAILURE)
	{
		mb_free(path);
		close(fd);
		mb_exit("Error\nCouldn't save texture paths.\n", EXIT_FAILURE);
	}
	mb_free(path);
	close(fd);
	return (EXIT_SUCCESS);
}

static int	check_element(t_game *game, char *element)
{
	char	identifier[3];

	while (element && *element && ft_strchr(" \t\r", *element))
		element++;
	if (element == NULL || *element == '\0' || ft_strlen(element) < 2)
		return (1);
	identifier[0] = *element++;
	identifier[1] = *element++;
	identifier[2] = '\0';
	if ((*element == ' ') && ft_strnstr("NOSOWEEA", identifier, 9) \
		&& !check_path(&(game->txt), element, identifier[0]))
		return (EXIT_SUCCESS);
	if (ft_strnstr("F C ", identifier, 5) && \
		!check_rgb(&(game->txt), element, identifier[0]))
		return (EXIT_SUCCESS);
	mb_exit("Error\nInvalid textures and/or RGB elements.\n", EXIT_FAILURE);
	return (EXIT_FAILURE);
}

int	check_elements(t_game *game, char *file)
{
	int		fd;
	char	*line;
	int		count;

	line = NULL;
	count = 0;
	fd = open(file, O_RDONLY);
	if (fd == -1)
		return (-1);
	while (count < TOTAL_ELEMENTS)
	{
		ft_gnl(fd, &line);
		mb_add(line);
		if (line && *line != '\n' && !check_element(game, line))
			count++;
		else if (!line || (*line != '\n' && *line != '\0'))
		{
			mb_free(line);
			close(fd);
			return (-1);
		}
		mb_free(line);
		line = NULL;
	}
	return (fd);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memorybook_utils.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:56:23 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 13:56:25 by pblagoje         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

/* private struct definition :
 * https://stackoverflow.com/questions/71724770/prototype-of-a-struct-in-c
 * it wasn't necessary after all :p I just added setters and getters here
 */

typedef void(*t_mb_func_exit)(void*);

typedef struct s_mb_exit
{
	t_mb_func_exit	f;
	void			*param;
}					t_mb_exit;

static t_mb_exit	*mb_get_exit_struct(void)
{
	static t_mb_exit	*mbexit = NULL;

	if (!mbexit)
		mbexit = mb_alloc(sizeof(t_mb_exit));
	return (mbexit);
}

void	mb_set_params_exit(void (*f)(void *), void *param)
{
	t_mb_exit	*mbexit;

	mbexit = mb_get_exit_struct();
	mbexit->param = param;
	mbexit->f = f;
}

void	mb_exec_exit_func(void)
{
	t_mb_exit	*mbexit;

	mbexit = mb_get_exit_struct();
	if (mbexit->f)
		mbexit->f(mbexit->param);
}

t_list	**mb_get_lst(void)
{
	static t_list	*lst = NULL;

	return (&lst);
}

int	mb_comp_addr(void *to_find, void *to_compare)
{
	return (to_find == to_compare);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memorybook_2d.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:55:48 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 18:32:14 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cube3d.h"

void	mb_add_2d(void **addr, int nb)
{
	int	i;

	i = 0;
	while (i < nb)
	{
		mb_add(addr[i]);
		i++;
	}
	mb_add(addr);
}

void	mb_free_2d(void **addr, int nb)
{
	int	i;

	i = 0;
	while (i < nb)
	{
		mb_free(addr[i]);
		i++;
	}
	mb_free(addr);
}

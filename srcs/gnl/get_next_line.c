/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <pblagoje@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/27 12:42:20 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 21:23:28 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

void	ft_free_gnl(char **s)
{
	if (*s != NULL)
	{
		free(*s);
		*s = NULL;
	}
}

char	*ft_strcpy(char *s1, char *s2)
{
	int			i;

	i = 0;
	while (s2[i])
	{
		s1[i] = s2[i];
		i++;
	}
	s1[i] = '\0';
	return (s1);
}

int	get_next_line(int fd, char **line)
{
	int			ret;
	static char	*mem;
	char		*buf;

	ret = 1;
	buf = NULL;
	if (BUFFER_SIZE <= 0 || !line || read(fd, buf, 0) == -1)
		return (-1);
	buf = malloc(sizeof(char) * (BUFFER_SIZE + 1));
	while (!ft_is_newline(mem, '\n') && ret != 0)
	{
		ret = read(fd, buf, BUFFER_SIZE);
		buf[ret] = '\0';
		mem = ft_strjoin_gnl(mem, buf);
	}
	if (!ret)
	{
		ft_free_gnl(&mem);
		ft_free_gnl(&buf);
		return (0);
	}
	*line = ft_strtrim_gnl(mem);
	mem = ft_strchr_gnl(mem, '\n');
	ft_free_gnl(&buf);
	return (1);
}

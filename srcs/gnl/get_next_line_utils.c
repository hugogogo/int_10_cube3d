/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <pblagoje@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 10:35:41 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 21:07:31 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int	ft_strlen_gnl(char *s)
{
	int		i;

	if (!s)
		return (0);
	i = 0;
	while (s[i])
		i++;
	return (i);
}

char	*ft_strjoin_gnl(char *s1, char *s2)
{
	char	*res;
	int		i;
	int		j;

	if (!s1 && !s2)
		return (NULL);
	i = 0;
	j = 0;
	res = malloc(sizeof(char) * (ft_strlen_gnl(s1) + ft_strlen_gnl(s2) + 1));
	if (!res)
		return (NULL);
	while (s1 != NULL && s1[i])
	{
		res[i] = s1[i];
		i++;
	}
	while (s2[j])
	{
		res[i + j] = s2[j];
		j++;
	}
	res[i + j] = '\0';
	ft_free_gnl(&s1);
	return (res);
}

int	ft_is_newline(char *s, int c)
{
	int		i;

	if (!s)
		return (0);
	i = 0;
	while (s[i])
	{
		if (s[i] == (unsigned char)c)
			return (1);
		i++;
	}
	return (0);
}

char	*ft_strchr_gnl(char *s, int c)
{
	int		i;
	int		is_newline;
	char	*tmp;

	if (!s)
		return (NULL);
	i = 0;
	is_newline = 0;
	while (s[i] && is_newline == 0)
	{
		if (s[i] == (unsigned char)c)
			is_newline = 1;
		i++;
	}
	tmp = malloc(sizeof(char) * (ft_strlen_gnl(&s[i]) + 1));
	if (!tmp)
		return (NULL);
	tmp = ft_strcpy(tmp, &s[i]);
	ft_free_gnl(&s);
	return (tmp);
}

char	*ft_strtrim_gnl(char *s)
{
	char	*res;
	int		size;
	int		i;

	size = 0;
	i = 0;
	while (s[size] != '\n')
		size++;
	res = malloc(sizeof(char) * (size + 1));
	if (!res)
		return (NULL);
	while (s[i] != '\n')
	{
		res[i] = s[i];
		i++;
	}
	res[i] = '\0';
	return (res);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cube3d_macro.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:34:39 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/05 14:52:41 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUBE3D_MACRO_H
# define CUBE3D_MACRO_H

/*
 * game params
 */

/* nbr pixel player move */
# define PLR_MV 10
/* degree of rotation of the player per press */
# define PLR_ROT 1
/* nbr key you can press at the same time */
# define MAX_NB_KEY 3
/* nbr of elements to parse in the .cub file (without the map) */
# define TOTAL_ELEMENTS 6
/* screen width */
# define SCREEN_WIDTH 1200
/* screen height */
# define SCREEN_HEIGHT 800
/* screen focal (in degree) */
# define SCREEN_FOCAL 70
/* size of a cell on the map */
# define CELL 100
// minimum time in milliseconds between two keypress
# define DEBOUNCE_TIME  10


/*
 * keys macro
 */

# define ARROW_LEFT			65361
# define ARROW_UP			65362
# define ARROW_RIGHT		65363
# define ARROW_DOWN			65364
# define KEY_A				97		// left
# define KEY_Q				113		// left (azerty)
# define KEY_D				100		// right
# define KEY_S				115		// backward
# define KEY_W				119		// forward
# define KEY_Z				122		// forward (azerty)
# define KEY_ESC			65307
# define KEY_SHIFT_LEFT		65505
# define KEY_SHIFT_RIGHT	65506
# define KEY_SPACE			32

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cube3d_proto.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:36:55 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 16:00:38 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUBE3D_PROTO_H
# define CUBE3D_PROTO_H

// -------------------------------
// SRC
// -------------------------------
// cube3d.c
void	destroy_mlx(void *param);
int		shut_down(void);

// -------------------------------
// SRC/INIT
// -------------------------------
// init_struct.c
t_game	*init_struct(void);
// init_game.c
void	init_game(t_game *game);
// init_textures.c
void	init_txtr(t_txt *txt, void *mlx_ptr);

// -------------------------------
// SRC/PARSING
// -------------------------------
// init_parsing.c
int		init_parsing(t_game *game, char *file);
// check_extension.c
int		check_extension(char *filename, char *ext);
// check_path.c
int		check_elements(t_game *game, char *file);
// check_rgb.c
int		check_rgb(t_txt *txt, char *element, char identifier);
// check_map_borders.c
int		check_map(t_map *map);
// check_map_content.c
int		check_content(t_map *map);

// -------------------------------
// SRC/HOOK
// -------------------------------
// keyhook.c
int		keypress(int keycode, t_game *game);
int		keyrelease(int keycode, t_game *game);
int		hook_action(t_game *game);
// key_action_1.c
int		is_esc(int *k_hook, int *is_action);
int		is_go_left(int *k_hook, int *is_action);
int		is_go_right(int *k_hook, int *is_action);
int		is_go_forward(int *k_hook, int *is_action);
int		is_go_backward(int *k_hook, int *is_action);
// key_action_2.c
int		is_turn_left(int *k_hook, int *is_action);
int		is_turn_right(int *k_hook, int *is_action);

// -------------------------------
// SRC/PLAYER
// -------------------------------
// player_moves.c
void	plr_posx_decrement(t_game *game, t_plr *plr);
void	plr_posy_decrement(t_game *game, t_plr *plr);
void	plr_posx_increment(t_game *game, t_plr *plr);
void	plr_posy_increment(t_game *game, t_plr *plr);
// player_rotates.c
void	rotate(t_plr *plr, t_coord *coord);
void	rotate_double(t_plr *plr, t_d_coord *coord);
void	plr_turn(t_plr *plr, int deg);
void	plr_turn_right(t_plr *plr);
void	plr_turn_left(t_plr *plr);
// player_limits.c
int		is_wall(t_game *game, int cell_x, int cell_y);
int		plr_out_limits(t_game *game, int x, int y);

// -------------------------------
// SRC/DRAW
// -------------------------------
// draw.c
void	draw_pixel(t_img *img, int x, int y, int color);
void	draw_line(t_img *img, t_vec *vec, int color);
void	draw(t_game *game);
// raycast.c
void	raycast(t_game *game, t_rcast *rcast);
// ray_intersect.c
void	ray_intersect_wall(t_game *game, t_rcast *rcast, t_vec *ray);
// draw_column.c
void	draw_column(t_game *game, t_rcast *rcast, t_wall *wall, t_txt *txt);

#endif

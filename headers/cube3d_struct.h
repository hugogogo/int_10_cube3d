/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cube3d_struct.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:31:38 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 21:36:13 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUBE3D_STRUCT_H
# define CUBE3D_STRUCT_H

/*
 * 3 structs with coordinates for
 * - simple precision point
 * - double precision point
 * - vector (two points)
 */

typedef struct s_d_coord
{
	double			x;
	double			y;
}					t_d_coord;

typedef struct s_coord
{
	int				x;
	int				y;
}					t_coord;

typedef struct s_vec
{
	t_coord			start;
	t_coord			end;
}					t_vec;

/*
 * struct with all elements for raycasting
 */

typedef struct s_wall
{
	t_vec			vec;
	int				height;
	int				limit;
	int				delta;
	int				posx;
	int				imgx;
}					t_wall;

/*
 * struct with all elements for raycasting
 */

typedef struct s_rcast
{
	t_vec			screen_size;
	t_vec			ray;
	t_wall			wall;
	int				screen_width;
	int				screen_height;
	int				screen_dist;
	int				cell;
	int				cell_x;
	int				cell_y;
	int				slope_x;
	int				slope_y;
	int				ray_sign_x;
	int				ray_sign_y;
	int				first_next_x;
	int				first_next_y;
	int				next_x;
	int				next_y;
	int				next_cell_x;
	int				next_cell_y;
	int				ray_step_x;
	int				ray_step_y;
	int				is_x;
	int				ray_nb;
	double			ratio;
}					t_rcast;

/*
 * structs for windows, and images associated
 */

typedef struct s_win
{
	void			*ptr;
	int				size_x;
	int				size_y;
}					t_win;

typedef struct s_img
{
	void			*ptr;
	char			*data;
	int				bpp;
	int				szl;
	int				ndn;
	int				height;
	int				width;
}					t_img;

/*
 * wall textures and floor/ceiling colors
 */

typedef struct s_txt
{
	char			*txt_north;
	char			*txt_south;
	char			*txt_east;
	char			*txt_west;
	int				rgb_floor;
	int				rgb_ceiling;
	t_img			img_n;
	t_img			img_s;
	t_img			img_e;
	t_img			img_w;
}					t_txt;

/*
 * structs for the map
 */

typedef struct s_map
{
	char			**content;
	char			*tmp_str;
	int				size_x;
	int				size_y;
	int				cell;
	int				plr_x;
	int				plr_y;
	int				plr_dir;
}					t_map;

/*
 * structs for the player and its moves
 */

typedef struct s_plr
{
	t_d_coord		exact;
	t_coord			pos;
	int				rot;
	int				deg;
	double			cosi;
	double			cosj;
	double			sini;
	double			sinj;
}					t_plr;

/*
 * struct with all elements for the project
 */

typedef struct s_game
{
	void						*mlx_ptr;
	t_win						win;
	t_img						img;
	t_plr						plr;
	t_rcast					rcast;
	t_txt						txt;
	t_map						map;
	int							fd;
	int							k_hook[MAX_NB_KEY];
	struct timeval  last_keypress_time;
}					t_game;

#endif

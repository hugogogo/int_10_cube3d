/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memorybook.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:35:47 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 17:39:33 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MEMORYBOOK_H
# define MEMORYBOOK_H

// memorybook.c
void	mb_init(void (*f)(void*), void *param);
void	*mb_alloc(size_t size);
void	mb_add(void *addr);
void	mb_free(void *addr);
void	mb_exit(char *str, int status);

// memorybook_2d.c
void	mb_add_2d(void **addr, int nb);
void	mb_free_2d(void **addr, int nb);

#endif

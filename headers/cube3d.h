/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cube3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pblagoje <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 13:34:54 by pblagoje          #+#    #+#             */
/*   Updated: 2022/05/04 15:57:22 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUBE3D_H
# define CUBE3D_H

# include "../libs/libft/includes/libft.h"
# include <mlx.h>
# include <unistd.h>	// for sleep()
# include <stdlib.h>	// for atoi()
# include <stdio.h> 	// for printf()
# include <math.h>      // for M_PI, cos(), sin()
# include <sys/stat.h>	// for open()
# include <fcntl.h>		// for open flags

# include "colors.h"
# include "memorybook.h"

# include "cube3d_macro.h"
# include "cube3d_struct.h"
# include "cube3d_proto.h"
# include "get_next_line.h"

# include <time.h>      // for time  
# include <sys/time.h>  // for gettimeofday

#endif

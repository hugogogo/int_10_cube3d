# run

**add minilibx and libft submodules :**
- git submodule init
- git submodule update

# todo

- resolve view buble
- add textures

# ressources

- [tuto mlx](https://harm-smits.github.io/42docs/libs/minilibx/getting_started.html)
- [tuto raycasting](https://lodev.org/cgtutor/raycasting.html)
- [tuto raycasting js](http://www.playfuljs.com/a-first-person-engine-in-265-lines)
- [tuto mlx and wolf3d](https://github.com/qst0/ft_libgfx)
- [tuto mlx images](https://github.com/keuhdall/images_example)
- [bmp images](https://web.archive.org/web/20080912171714/http://www.fortunecity.com/skyscraper/windows/364/bmpffrmt.html)
- [bmp images in c](https://stackoverflow.com/questions/2654480/writing-bmp-image-in-pure-c-c-without-other-libraries)


---
# keys numbers

```
+-------------.-------.-------+------.-------.-------+------.-------.-------+
| keys          linux   mac   | keys   linux   mac   | keys   linux   mac   |
|-------------.-------.-------|------.-------.-------|------.-------.-------+
| direction keys              | alpha keys           | num keys             |
|-------------.-------.-------|------.-------.-------|------.-------.-------+
| w forward   . 119   . _____ | a    . 97    . _____ | & (0). (38)  . _____ |
| a left      . 97    . _____ | b    . 98    . _____ | é (1). (233) . _____ |
| s backward  . 115   . _____ | c    . 99    . _____ | " (2). (34)  . _____ |
| d right     . 100   . _____ | d    . 100   . _____ | ' (3). (39)  . _____ |
|-------------.-------.-------| e    . 101   . _____ | ( (4). (40)  . _____ |
| arrows keys                 | f    . 102   . _____ | - (5). (45)  . _____ |
|-------------.-------.-------| g    . 103   . _____ | è (6). (232) . _____ |
| ←           . 65361 . _____ | h    . 104   . _____ | _ (7). (95)  . _____ |
| →           . 65363 . _____ | i    . 105   . _____ | ç (8). (231) . _____ |
| ↓           . 65364 . _____ | j    . 106   . _____ | à (9). (224) . _____ |
| ↑           . 65362 . _____ | k    . 107   . _____ |------.-------.-------|
|-------------.-------.-------| l    . 108   . _____ | other keys           |
| special keys                | m    . 109   . _____ |------.-------.-------|
|-------------.-------.-------| n    . 110   . _____ | < (>). 60    . _____ |
| esc         . 65307 . _____ | o    . 111   . _____ | >    .(60)   . _____ |
| suppr       . 65535 . _____ | p    . 112   . _____ | space. 32    . _____ |
| delete      . 65288 . _____ | q    . 113   . _____ | tab  . 65289 . _____ |
| enter       . 65293 . _____ | r    . 114   . _____ | = (+). 61    . _____ |
|-------------.-------.-------| s    . 115   . _____ | +    .(61)   . _____ |
| hot keys                    | t    . 116   . _____ | -    . 45    . _____ |
|-------------.-------.-------| u    . 117   . _____ |      .       .       |
| shift left  . 65505 . _____ | v    . 118   . _____ |      .       .       |
| shift right . 65506 . _____ | w    . 119   . _____ |      .       .       |
| ctrl left   . 65507 . _____ | x    . 120   . _____ |      .       .       |
| ctrl right  . 65508 . _____ | y    . 121   . _____ |      .       .       |
|             .       .       | z    . 122   . _____ |      .       .       |
+-------------.-------.-------+------.-------.-------+------.-------.-------+
```

---
# hook key masks

- https://tronche.com/gui/x/xlib/events/keyboard-pointer/keyboard-pointer.html

```
x_event |   x_mask | action
-------------------------------------------------
      2 | 1L << 0  | key press
      3 | 1L << 1  | key release
      4 |          | mouse press
      5 |          | mouse release
      6 |          | mouse move
     12 |          | expose event
     17 | 1L << 17 | x button press (red button)
```

---
# fonctions externes autorisees :

- **open**
- **close**
- **read**
- **write**
- **malloc**
- **free**
- **perror**
- **strerror**
- **exit**
- **math lib** :
    - **lm** -> needed at compilation to link the lib (eg: `gcc foo.c -o foo -lm`)
    - **man**
    - **man 3 math**
- **minilibx** :
    - `minilibx_opengl.tgz`
    - `minilibx_mms_20200219_beta.tgz`
    - **to open an archive.tgz** :
        - `gzip -d archive.tgz` -> turn it into archive.tar
        - `tar -xf archive.tar` -> un-archive it
    - how to add a man directory to the manual :
        - cp man/man1 /usr/local/share/man/man1 (create man1 if necessary)
        - mandb
    - i didn't use any of both library above but the one for linux : https://github.com/42Paris/minilibx-linux (there are pbm with their man pages)

